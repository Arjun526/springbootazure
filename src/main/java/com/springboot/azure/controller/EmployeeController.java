package com.springboot.azure.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.azure.model.Employee;
import com.springboot.azure.repository.EmployeeRepository;

@RestController
//@RequestMapping("/rest")
public class EmployeeController {

	@Autowired
	private EmployeeRepository repository;
	
	@RequestMapping(value="/product", method = RequestMethod.POST)
	public Employee addEmployee(@RequestBody Employee employee) {
		Employee emp = repository.save(employee);
		return emp;
		
	}
	@GetMapping("/produts")
	public List<Employee> getEmployees(){
		return repository.findAll();
	}
}
