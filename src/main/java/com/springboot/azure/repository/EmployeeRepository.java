package com.springboot.azure.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springboot.azure.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

}
