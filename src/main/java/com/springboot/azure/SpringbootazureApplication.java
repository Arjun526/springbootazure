package com.springboot.azure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootazureApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootazureApplication.class, args);
	}

}
